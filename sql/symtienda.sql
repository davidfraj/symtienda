-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-05-2018 a las 18:54:46
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `symtienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20180504164950'),
('20180507170716');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticia`
--

CREATE TABLE `noticia` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `noticia`
--

INSERT INTO `noticia` (`id`, `titulo`, `descripcion`, `autor`, `fecha`) VALUES
(1, 'PSOE y Ciudadanos pactan bonificar al 99% las herencias de hasta 500.000 €', 'El Gobierno PSOE-CHA y Ciudadanos han pactado finalmente bonificar al 99% el impuesto de Sucesiones para cónyuges y hijos que hereden hasta 500.000 euros, independientemente del patrimonio que tengan. La reforma fiscal, que requerirá ahora de la preceptiva mayoría parlamentaria, implica un recorte de 40 millones de euros en la recaudación, que batió el récord el año pasado al alcanzar los 200 millones el año pasado.\r\n\r\nEl consejero de Hacienda, Fernando Gimeno, y su equipo han requerido hasta cinco reuniones con el portavoz del área de C’s en las Cortes de Aragón, Javier Martínez, para cerrar un acuerdo por el que solo pagarán el impuesto los 167 contribuyentes que heredan grandes patrimonios. Así  lo acaba de anunciar el diputado, quien ha destacado que esta reforma fiscal supone la “supresión” efectiva del impuesto para las clases medias.\r\n\r\n \r\nPodemos e IU censuran el giro del PSOE por su pacto en Sucesiones con C\'s\r\n\r\nLeer más\r\nFuentes oficiales de la DGA han asegurado este lunes que la propuesta se llevará esta misma semana a la Cámara para su tramitación, aunque aún está por ver si entrará en vigor en el segundo semestre o se demora al próximo ejercicio fiscal. Lo que sí han aclarado que los que no pagan ahora el impuesto tampoco lo harán con la reforma, dado que el mínimo exento se quedará al menos en los actuales 150.000 euros para los que tengan un patrimonio de hasta 400.000 euros.', 'david', '2018-05-07 00:00:00'),
(2, 'Los sorteos para determinar qué niños entran, entre las principales reclamaciones en la escolarización', 'Los sorteos para decidir qué niños son admitidos y cuáles quedan excluidos en los colegios aragoneses que han tenido más plazas que vacantes van a estar este año entre las principales reclamaciones de los padres.\r\n\r\nEsta es al menos la percepción y el temor que se tiene en algunos centros y ampas consultadas, en base a lo ocurrido en las últimas horas. Hay que recordar que los desacuerdos por las listas provisionales de admitidos y excluidos que los centros hicieron públicas el viernes pasado pueden presentarse desde este lunes y hasta el próximo miércoles 9.', 'heraldo', '2018-05-06 00:00:00'),
(3, 'El Huesca solo quiere pensar en el Alcorcón', 'Quizás sea una de las frases más manidas en el fútbol español, pero el Huesca la hace suya de cara al tramo final de la competición, en la que apela al \"partido a partido\". Así lo aseguraba Rubi en su comparecencia previa al viaje a Córdoba, y así lo ha inculcado a su vestuario, al que este domingo puso voz Gallar, hablando también del trabajo diario y sobre todo, de no hacer cuentas que pasen por mirar lo que suceda en otros campos.', 'heraldo', '2018-05-06 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unidades` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `nombre`, `unidades`, `precio`) VALUES
(1, 'impresora', 7, '120.50'),
(2, 'impresora', 7, '120.50'),
(3, 'impresora', 7, '120.50'),
(4, 'impresora', 7, '120.50'),
(5, 'impresora', 7, '120.50'),
(6, 'impresora', 7, '120.50'),
(7, 'impresora', 7, '120.50'),
(8, 'impresora', 7, '120.50'),
(9, 'impresora', 7, '120.50'),
(10, 'impresora', 7, '120.50');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `noticia`
--
ALTER TABLE `noticia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `noticia`
--
ALTER TABLE `noticia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
