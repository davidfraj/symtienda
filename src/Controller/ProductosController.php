<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

//Incluyo la clase Producto, que he creado con doctrine
use App\Entity\Producto;

class ProductosController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {

        //Quiero crearme un producto,asi que creo un objeto de la clase Producto
        $producto=new Producto();
        $producto->setNombre('impresora');
        $producto->setPrecio(120.5);
        $producto->setUnidades(7);

        //Para poder PERSISTIR los datos, necesito llamar al ENTITY MANAGER de Doctrine
        $entityManager = $this->getDoctrine()->getManager();

        //Una vez que accedo al EntityManager, lo uso para guardar el producto en cache
        $entityManager->persist($producto);
        //Y luego lo GUARDO en BBDD
        $entityManager->flush();

        return $this->render('productos/index.html.twig', [
            'controller_name' => 'Titulo de productos controller',
        ]);
    }

    /**
     * @Route("/producto/{id}", name="producto")
     */
    public function producto($id)
    {
        return $this->render('productos/producto.html.twig', [
            'controller_name' => 'Quiero mostrar un unico producto'.$id,
        ]);

    }

}
