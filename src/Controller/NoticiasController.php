<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

use Symfony\Component\HttpFoundation\Request;

use App\Entity\Noticia;

class NoticiasController extends Controller
{
    /**
     * @Route("/noticias", name="noticias")
     */
    public function index()
    {
    	//Quiero rellenar las noticias, para pasarselas a la vista
    	//Para eso llamo a lo que se conoce por un REPOSITORIO de la Class 
    	//que queremos tratar
    	$repository = $this->getDoctrine()->getRepository(Noticia::class);

    	//Una vez, que accedemos a dicho Repositorio, traemos las noticias
    	$noticias=$repository->findAll();

    	//Esto es un ejemplo de calcular valores con funciones php
    	$ultima=time();

    	//Pinto la vista
        return $this->render('noticias/index.html.twig', [
            'controller_name' => 'NoticiasController', 
            'titulo'=>'Noticias de la web',
            'fecha'=>date('d/m/Y'),
            'ultimaVisita'=>$ultima,
            'noticias'=>$noticias
        ]);
    } //Fin de la function index()


    /**
     * @Route("/noticias/{id}", name="noticias_detalle")
     */
    public function detalle($id)
    {
        //Uso mi gestor de Entidades y repositorios, para traer Noticias
        $repository = $this->getDoctrine()->getRepository(Noticia::class);

        //traigo una UNICA noticia
        $noticia=$repository->find($id);

        //Pinto la vista
        return $this->render('noticias/detalle.html.twig', [
            'noticia'=>$noticia
        ]);

    } //Fin de la function detalle()


    /**
     * @Route("/noticias_insertar", name="noticias_insertar")
     */
    public function insertar(Request $request)
    {
        //https://symfony.com/doc/current/forms.html

        //Me creo un objeto de la class Noticia
        $noticia=new Noticia();

        //Basandome en dicho objeto, me creo un formulario
        $formulario = $this->createFormBuilder($noticia)
            ->add('titulo', TextType::class)
            ->add('descripcion', TextareaType::class)
            ->add('autor', TextType::class)
            ->add('fecha', DateTimeType::class)
            ->add('enviar', SubmitType::class)
            ->getForm();

        //Manejo el request del formulario
        $formulario->handleRequest($request);

        if ($formulario->isSubmitted() && $formulario->isValid()) {
            
            //Relleno $noticia, con los datos del formulario
            $noticia = $formulario->getData();

            //Guardo dicha noticia en la bbdd
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($noticia);
            $entityManager->flush();

            //Si quiero puedo redirigir, a un path en concreto
            return $this->redirectToRoute('noticias');
        }

        //Pinto la vista
        return $this->render('noticias/insertar.html.twig', [
            'formulario' => $formulario->createView()
        ]);

    } //Fin de la function insertar()


    /**
     * @Route("/noticias_borrar/{id}", name="noticias_borrar")
     */
    public function borrar($id)
    {
        //Uso mi gestor de repositorios, para traer Noticias
        $repository = $this->getDoctrine()->getRepository(Noticia::class);

        //traigo una UNICA noticia
        $noticia=$repository->find($id);

        //Uso mi gestor de Entidades
        $entityManager = $this->getDoctrine()->getManager();
        //LE digo al gestor de entidades que quiero borrar dicha noticia
        $entityManager->remove($noticia);
        //REalizo las consultas sobre la BBDD que estan pendientes en CAche
        $entityManager->flush();

        //Si quiero puedo redirigir, a un path en concreto
        return $this->redirectToRoute('noticias');

    } //Fin de la function borrar


    /**
     * @Route("/noticias_modificar/{id}", name="noticias_modificar")
     */
    public function modificar(Request $request, $id)
    {
        //Uso mi gestor de repositorios, para traer la noticia
        $repository = $this->getDoctrine()->getRepository(Noticia::class);
        $noticia=$repository->find($id);

        //Me creo un formulario, usando dicha noticia
        $formulario = $this->createFormBuilder($noticia)
            ->add('titulo', TextType::class)
            ->add('descripcion', TextareaType::class)
            ->add('autor', TextType::class)
            ->add('fecha', DateTimeType::class)
            ->add('enviar', SubmitType::class)
            ->getForm();

        //Manejo el request del formulario
        $formulario->handleRequest($request);

        if ($formulario->isSubmitted() && $formulario->isValid()) {
            
            //Relleno $noticia, con los datos del formulario
            $noticia = $formulario->getData();

            //Guardo dicha noticia en la bbdd
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($noticia);
            $entityManager->flush();

            //Si quiero puedo redirigir, a un path en concreto
            return $this->redirectToRoute('noticias');
        }

        //Pinto la vista
        return $this->render('noticias/insertar.html.twig', [
            'formulario' => $formulario->createView()
        ]);
    
    } //Fin de la funtion modificar

}
